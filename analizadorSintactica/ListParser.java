public class ListParser extends Parser {
    public ListParser (Lexer input){
        super(input);
    }

    public void list(){
        match(ListLexer.LBRACK);
        elements();
        match(ListLexer.RBRACK);
    }

    void elements(){
        element();
        while (lookhead.type==ListLexer.COMMA) {
            match(ListLexer.COMMA);
            element();
        }
    }

    void element(){
        if(lookhead.type == ListLexer.NAME)
            match(ListLexer.NAME);
        else if(lookhead.type== ListLexer.LBRACK)
            list();
        else
            throw new Error("Expecting name or list; found " + lookhead);
    }
}
