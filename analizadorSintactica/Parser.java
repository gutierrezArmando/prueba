public class Parser{
    Lexer input;
    Token lookhead;

    public Parser(Lexer input){
        this.input = input;
        lookhead = input.nextToken();
    }

    public void match(int x)
    {
        if(lookhead.type == x)
            consume();
        else
            throw new Error ("Expecting" + input.getTokenName(x)+ "; found " + lookhead);
    }

    public void consume()
    {
        lookhead = input.nextToken();
    }

}
